#!/usr/bin/env python
import sqlite3
import datetime

from getpass import getpass

from sevenparkdata import settings
from sevenparkdata.provider.auth import HttpBasicAuthProvider


def create_user_table(cursor):
    cursor.execute("""
        create table if not exists user(
            id integer primary key,
            username text unique,
            password text
        )
    """)

def create_user_prompt(cursor):
    if raw_input("Create user ([Y] or N)? ").strip().upper() == 'N':
        return

    username = raw_input("Username: ").strip().lower() 
    while True:
        password = getpass("Password: ").strip()
        confirmed = getpass("Confirm Password: ").strip()

        if password == confirmed:
            break
        else:
            print "ERROR: Passwords do not match!\n"
    
    auth_provier = HttpBasicAuthProvider(database=None)
    password_hash = auth_provier._hash_password(password)
    cursor.execute("insert into user (username, password) values(?, ?)",
                    (username, password_hash))


if __name__ == '__main__':
    db = sqlite3.connect(settings.DATABASE)
    cursor = db.cursor()

    create_user_table(cursor)
    create_user_prompt(cursor)

    db.commit()
    db.close()
