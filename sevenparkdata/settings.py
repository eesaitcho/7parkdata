DATABASE = '/tmp/sevenparkdata_stock.db'

SECRET_KEY = ')=6ueb60l-$^=b)1$)7cj)n7e=)ey9wb+9-**zs0($l7r*qyy$'


# cache configurations
SIMPLE_CACHE_CONFIG = {
    'CACHE_TYPE': 'simple',
    'CACHE_DEFAULT_TIMEOUT': 86400,
}
CACHE_CONFIG = SIMPLE_CACHE_CONFIG

# user auth provider
AUTH_PROVIDER = 'sevenparkdata.provider.auth.HttpBasicAuthProvider'

# stock symbol and price lookup providers
SYMBOL_QUERY_PROVIDER = 'sevenparkdata.provider.stockapi.ModStockSymbolProvider'
STOCK_PRICE_PROVIDER = 'sevenparkdata.provider.stockapi.ModStockPriceProvider'
