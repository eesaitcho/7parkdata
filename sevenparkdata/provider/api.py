import abc

from sevenparkdata.provider import ProviderError


class APIProviderConnectError(ProviderError):
    """Failure to connect error for API Provider"""
    pass


class APIProviderResourceNotFound(ProviderError):
    """No resources found error for API Provider"""
    pass


class APIProviderBadQuery(ProviderError):
    """Improper query error for API Provider"""
    pass


class BaseAPIProvider(object):
    """Base class for API providers"""
    __metaclass__ = abc.ABCMeta

    def create(self, **params):
        """Create an API resource instance"""
        raise NotImplementedError

    def delete(self, subject, **params):
        """Delete an API resource instance"""
        raise NotImplementedError

    def get(self, subject, **params):
        """Get an API resource instance"""
        raise NotImplementedError

    def query(self, **params):
        """Query for API resource instances"""
        raise NotImplementedError

    def update(self, subject, **params):
        """Update an API resource instance"""
        raise NotImplementedError
