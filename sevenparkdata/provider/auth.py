import abc
import base64
import hashlib
import random


DEFAULT_PASSWORD_PEPPER = 'sevenparkdata'
DEFAULT_ALGORITHM = 'sha512'


class BaseAuthProvider(object):
    """Base class for Auth providers"""
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def check(self, request):
        """
        Checks if current call is authorized

        Args:
            request (Request): flask request object for current call

        Returns:
            (bool): results of authorization check
        """
        raise NotImplementedError


class HttpBasicAuthProvider(BaseAuthProvider):
    """Provider for HTTP Basic Auth"""
    def __init__(self, **params):
        self.database = params.pop('database')
        self.algorithm = params.pop('algorithm', DEFAULT_ALGORITHM)
        self.pepper = params.pop('pepper', DEFAULT_PASSWORD_PEPPER)

    def check(self, request):
        """
        Checks if current call is authorized

        Args:
            request (Request): flask request object for current call

        Returns:
        """
        authorization = request.authorization
        if not authorization:
            return False

        username = authorization.username
        password = authorization.password

        cursor = self.database.cursor()
        cursor.execute("select password from user where username = ?",
                       (username,))

        rows = cursor.fetchall()
        password_hash = rows and rows[0][0]

        if not password_hash:
            return False

        return self._check_password(password_hash, password)

    def _check_password(self, password_hash, password):
        """Compares password with hash"""
	algorithm, salt, digest = password_hash.split('$')
        return digest == self._generate_digest(salt, password, algorithm)

    def _hash_password(self, password):
        """Hashes a password"""
	salt = base64.b64encode(str(u'%0x' % random.getrandbits(37)))[:6]
        algorithm = self.algorithm
        digest = self._generate_digest(salt, password)
        return '%s$%s$%s' % (algorithm, salt, digest)

    def _generate_digest(self, salt, password, algorithm=None):
        """Generates a digest"""
        algorithm = algorithm or self.algorithm
        text = ':'.join([self.pepper, salt, password])

        if algorithm in ('sha256', 'sha512', 'sha1',):
            hashfunc = getattr(hashlib, algorithm)
            return hashfunc(text.encode('utf-8')).hexdigest()
        else:
            raise NotImplementedError
