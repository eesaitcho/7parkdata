import abc

import requests
from sevenparkdata.provider import api


MOD_QUOTE_URL = 'http://dev.markitondemand.com/MODApis/Api/v2/Quote/json'
MOD_LOOKUP_URL = 'http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json'


class BaseStockPriceProvider(api.BaseAPIProvider):
    """A stock price API provier"""
    def get(self, subject, **params):
        """
        Args:
            subject (str): stock symbol whose price to fetch

        Returns:
            (dict): a dictionary of with the following keys - {
                symbol: (str),
                name: (str),
                price: (float),
                timestamp: (datetime)
            }
        """
        data = self._get_price(subject)

        return data

    @abc.abstractmethod
    def _get_price(self, symbol):
        raise NotImplementedError


class BaseStockSymbolProvider(api.BaseAPIProvider):
    """A stock symbol API provier"""
    def query(self, **params):
        """
        Keyward args:
            name (str): company name whose symbol to lookup
            limit (int): the number of instances to return. defaults to 10

        Returns:
            (list): a list of dictionaries with the following keys -
                {symbol: (str), name: (str)}
        """
        name = params.pop('name')
        limit = params.pop('limit', 10)
        data = self._query_symbols(name, limit)

        return data

    @abc.abstractmethod
    def _query_symbols(self, name, limit):
        raise NotImplementedError


class ModStockPriceProvider(BaseStockPriceProvider):
    """Markit On Demand stock price provider"""
    def __init__(self, *args, **kwargs):
        self.url = kwargs.pop('url', MOD_QUOTE_URL)
        super(ModStockPriceProvider, self).__init__(*args, **kwargs)

    def _get_price(self, symbol):
        params = {'symbol': symbol}
        response = requests.get(self.url, params=params)
        if not response.ok:
            raise api.APIProviderConnectError(
                "Error requesting data on url: %s" % response.url)

        data = response.json()
        if 'message' in data or data.get('Status') != 'SUCCESS':
            raise api.APIProviderResourceNotFound(
                "Price not found for stock: %s" % symbol)

        return {
            'symbol': data['Symbol'],
            'name': data['Name'],
            'price': data['LastPrice'],
            'timestamp': data['Timestamp']
        }


class ModStockSymbolProvider(BaseStockSymbolProvider):
    """Markit On Demand stock symbol provider"""
    def __init__(self, *args, **kwargs):
        self.url = kwargs.pop('url', MOD_LOOKUP_URL)
        super(ModStockSymbolProvider, self).__init__(*args, **kwargs)

    def _query_symbols(self, name, limit):
        params = {'input': name}
        response = requests.get(self.url, params=params)
        if not response.ok:
            raise api.APIProviderConnectError(
                "Error requesting data on url: %s" % response.url)

        data = response.json()
        if not isinstance(data, list):
            raise api.APIProviderBadQuery(
                "Improper query for stock name: %s" % name)

        return [
            {'symbol': item['Symbol'], 'name': item['Name']}
            for item in data[:limit]
        ]
