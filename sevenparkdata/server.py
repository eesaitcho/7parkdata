#!/usr/bin/env python
import importlib
import flask
import sqlite3
import flask_cache

from sevenparkdata import settings
from sevenparkdata.manager import stockdata
from sevenparkdata.manager import auth


app = flask.Flask(__name__)


def _get_obj_from_path(path):
    parts = path.split('.')
    obj = parts[-1]
    module_path = '.'.join(parts[:-1])

    module = importlib.import_module(module_path)
    return getattr(module, obj)


def get_auth_manager():
    """Returns the configured manager instance for auth"""
    g = flask.g
    manager = getattr(g, '_auth_manager', None)

    if manager is None:
        provider = _get_obj_from_path(settings.AUTH_PROVIDER)
        manager = g._auth_manager = auth.AuthManager(
            provider, database=get_db())

    return manager


def get_db():
    """Returns the configured database instance"""
    g = flask.g
    db = getattr(g, '_database', None)

    if db is None:
        db = g._database = sqlite3.connect(settings.DATABASE)
    return db


def get_cache():
    """Returns the configured cache instance"""
    g = flask.g
    cache = getattr(g, '_cache', None)

    if cache is None:
        cache = g._cache = flask_cache.Cache(app, config=settings.CACHE_CONFIG)

    return cache


def get_stockdata_manager():
    """Returns the configured manager instance for stockdata"""
    g = flask.g
    manager = getattr(g, '_stockdata_manager', None)

    if manager is None:
        price_provider = _get_obj_from_path(settings.STOCK_PRICE_PROVIDER)
        symbol_provider = _get_obj_from_path(settings.SYMBOL_QUERY_PROVIDER)

        cache = get_cache()
        manager = g._stockdata_manager = stockdata.StockDataManager(
            price_provider, symbol_provider, cache)

    return manager


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes db connection at end of request call"""
    db = getattr(flask.g, '_database', None)
    if db is not None:
        db.close()


@app.route('/api/1/stock/<symbol>', methods=['GET'])
@auth.auth_protected(get_auth_manager)
def stock(symbol):
    """Get Stock resource

    Return attributes:
        symbol (str) - stock symbol. resource identifier
        name (str) - stock's company name
        price (float) - stock's price
        timestamp (str) - timestamp of stock price
        url (str) - url to resource
    """
    manager = get_stockdata_manager()

    try:
        instance = manager.get_price(symbol)
    except stockdata.StockDataError:
        errors = ["Resource not found"]
        response = flask.jsonify(errors=errors)
        response.status_code = 404
    else:
        resource = {'url': flask.request.path}
        resource.update(instance)
        response = flask.jsonify(resource)

    return response


@app.route('/api/1/stock', methods=['GET'])
@auth.auth_protected(get_auth_manager)
def stock_query():
    """Get Stock resources

    Arguments:
        name: str

    Return attributes -
        resources (list<Stock>) - a list of Stock resource
        total (int) - total number of resources returned
    """
    manager = get_stockdata_manager()
    request = flask.request

    name = request.args.get('name')

    errors = []
    if name is None:
        errors.append("Missing query parameter 'name'")
    if errors:
        response = flask.jsonify(errors=errors)
        response.status_code = 406
        return response

    results = manager.get_symbols(name.lower())

    path = flask.request.path
    resources = []
    for item in results:
        resource = {
            'price': None,
            'timestamp': None,
            'url': '%s/%s' % (path, item['symbol'])
        }
        resource.update(item)
        resources.append(resource)

    return flask.jsonify(resources=resources, total=len(resources))


if __name__ == '__main__':
    app.run(debug=True)
