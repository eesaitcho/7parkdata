import json

from sevenparkdata.provider import ProviderError


DEFAULT_CACHE_PREFIX = 'sevenparkdata.stockdata'


class StockDataError(Exception):
    """Base Stock Data Error"""
    pass


class StockDataManager(object):
    """A Stock Data Manager class"""
    def __init__(
            self, price_provider=None, symbol_provider=None, symbol_cache=None,
            cache_prefix=DEFAULT_CACHE_PREFIX):
        """
        Keyword args:
            price_provider (BaseStockPriceProvider): source of stock prices
            symbol_provider (BaseStockSymbolProvider): source of stock symbol
            symbol_cache (FlaskCache): optional object to cache symbol data
            cache_prefix (str): prefix for cache keys. Default value is
                sevenparkdata.stockdata.DEFAULT_CACHE_PREFIX
        """
        self.price_provider = price_provider()
        self.symbol_provider = symbol_provider()
        self._cache = symbol_cache
        self._cache_prefix = cache_prefix

    def get_price(self, symbol):
        """
        Args:
            symbol (str): the symbol of the stock to lookup

        Returns:
            (dict): a dictionary of with the following keys -
                {symbol: (str), name: (str), price: (float), timestamp: (str)}
        """
        price_provider = self.price_provider
        if not price_provider:
            raise StockDataError("Missing price provider")

        try:
            result = price_provider.get(symbol)
        except ProviderError as error:
            raise StockDataError(str(error))

        return result


    def get_symbols(self, name):
        """
        Args:
            name (str): company name of stock symbol to query

        Returns:
            (list): a list of dictionary of with the following keys -
                {symbol: (str), name: (str)}
        """
        cache_value = self._retrieve_from_cache(name)
        if cache_value:
            return cache_value

        symbol_provider = self.symbol_provider
        if not symbol_provider:
            raise StockDataError("Missing symbol provider")

        try:
            symbols = symbol_provider.query(name=name, limit=10)
        except ProviderError as error:
            raise StockDataError(str(error))

        if symbols:
            self._store_to_cache(name, symbols)

        return symbols

    def _compose_cache_key(self, identifier):
        return "%s:%s" % (self._cache_prefix, identifier.lower())

    def _retrieve_from_cache(self, identifier):
        cache = self._cache
        if cache:
            key = self._compose_cache_key(identifier)
            value = cache.get(key)
            if value:
                return json.loads(value)

    def _store_to_cache(self, identifier, value):
        cache = self._cache
        if cache:
            key = self._compose_cache_key(identifier)
            cache.set(key, json.dumps(value))
