import flask
import functools


def auth_protected(get_manager_callable):
    """
    Decorator to apply authentication to view functions

    Args:
        get_manager_callable (callable): function to fetch an instance of an
            AuthManager

    Returns:
        (function): decorated view function
    """
    def wrapper(func):
        @functools.wraps(func)
        def view_wrapper(*args, **kwargs):
            manager = get_manager_callable()
            if not manager.check(flask.request):
                errors = ["Not Authorized"]
                response = flask.jsonify(errors=errors)
                response.status_code = 401
                response.headers['WWW-Authenticate'] = (
                    'Basic realm="Login required"')
                return response

            return func(*args, **kwargs)

        return view_wrapper

    return wrapper


class AuthManager(object):
    """
    Auth Manager class. Provides a plugable framework for managing
    authentication and authorization through customizable BaseAuthProvider
    objects (see sevenparkdata.providers.auth for details)

    """
    def __init__(self, provider, **params):
        """
        Args:
            provider(BaseAuthProvider) - provider for authentication

        Kwargs:
            provider specific params to pass in
        """
        self.provider = provider(**params)

    def check(self, request):
        """
        Check authorization against provider
        """
        return self.provider.check(request)
