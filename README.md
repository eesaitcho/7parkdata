Title: 7Park Data Challenge
Author: Shola Esho


# 7Park Data Challenge

1. [Overview](#overview)
2. [Setup](#setup)
3. [Usage](#usage)
4. [Questions](#questions)


## 1. Overview

This application supplies an API that allows for querying of stock symbol by
name and retreving of stock price quotes.


## 2. Setup

Install the requirements:

    $ pip install -r requirements.pip

Run the configuration script below. This will setup the local database and prompt for the creation of an application user. The script can be repeatedly ran to create multiple users while persisting existing users.

    $ ./setup py

Launch the application with this command:

    $ ./sevenparkdata/server.py

Access it locally at http://127.0.0.1:5000


## 3. Usage

### Authentication

All API endpoints within this application require authorization for all requests. Users created from Step (#2.b) suffice as valid credentials. Here's an example call using curl with the username `joe` and password `supersecret`:

    $ curl -u joe:supersecret http://127.0.0.1:5000/api/1/stock/MSFT

### API Resources:

#### Show Stock
----
  Returns json data about a single stock

* **URL**

  /api/1/stock/:symbol

* **Method:**

  `GET`

* **URL Params**

  **Required**:

  `symbol=[string]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ name : "Microsoft Corp", price : 58.03, symbol : "MSFT", timestamp : "Fri Aug 26 15:59:00 UTC-04:00 2016", url : "/api/1/stock/MSFT" }`

* **Error Response:**
  * **Code:** 404 NOT FOUND <br />
    **Content**: `{ errors : [ "Resource not found" ] }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content**: `{ errors: [ "Not Authorized" ] }`

#### Search Stock
----
  Returns json data about multiple stocks

* **URL**

  /api/1/stock

* **Method:**

  `GET`

* **URL Params**

  None

* **Data Params**

  **Required**:

  `name=[string]`


* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[ { name : "Microsoft Corp", price : null, symbol : "MSFT", timestamp : null, url : "/api/1/stock/MSFT" ], total : 1 }`

* **Error Response:**
  * **Code:** 406 NOT ACCEPTABLE <br />
    **Content**: `{ errors : [ "Missing query parameter 'name'" ] }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content**: `{ errors: [ "Not Authorized" ] }`


## 4. Questions

#### Question #1
If you were to deploy this in a load-balanced setting, how would you modify the API so the user’s authenticated session is transferred between servers in the load-balancer?

#### Answer #1
Since the API is stateless and thus sessionless, there will be no need to maintain an authenticated session. In the alternate case, one could store the session in a centralized storage such as a database or caching service in which all nodes of the cluser have access.

I approached the initial problem with a solution that allows for plugable parts (the Manager-Provider concept). This pattern is in place for authentication and stock data lookup. In the case where authentication is moved from the node instances, a new AuthProvider will be written to replace the HttpBasicAuthProvider and to support local auth functionality while utilizing the centralized session lookups.
